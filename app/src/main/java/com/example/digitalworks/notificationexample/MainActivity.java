package com.example.digitalworks.notificationexample;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {
    Button button;
    public static Notification.Builder notification_builder;
    public static NotificationManager notification_manager;
    public static RemoteViews contentView;
    public static RemoteViews contentViewEx;
    public static Notification notification =null;



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                sendNotification();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    notification = notification_builder.build();
                    notification_manager.notify(1,notification);
                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void sendNotification() {

        //Get an instance of NotificationManager//

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String chanel_id = "3000";
            CharSequence name = "Channel Name";
            String description = "Chanel Description";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(chanel_id, name, importance);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.BLUE);
            notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notification_manager.createNotificationChannel(mChannel);
            notification_builder = new Notification.Builder(this, chanel_id);
        } else {
            notification_builder = new Notification.Builder(this);
        }

        String url = "http://www.google.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);


        contentView = new RemoteViews(getPackageName(), R.layout.notification_custom);
        contentViewEx = new RemoteViews(getPackageName(), R.layout.notification_custom_expanded);

        notification_builder.setSmallIcon(R.drawable.ic_launcher_foreground)
                .setCustomContentView(contentView)
                .setCustomBigContentView(contentViewEx)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setVibrate(new long[0]);



    }
}
